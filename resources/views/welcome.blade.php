<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Inventory Management</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('assets/css/theme.css')}}">

        <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery-3.6.0.min.js') }}"></script>
        <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/bootstrap.bundle.min.js') }}"></script>
        <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/jquery.dataTables.js') }}"></script>
        <script type="text/javascript" charset="utf8" src="{{ asset('assets/js/sweetalert2@11.js') }}"></script>

        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="">
        <div class="container">
            <div class="card mt-3">
                <div class="card-header">
                    {{ __("Inventory Management") }} <span class="float-end">[ Designed By Namvariii@gmail.com ]</span>
                </div>
                <div class="card-body">
                    <div style="direction: rtl; text-align: right">
                        <h3 class="fw-bold mb-4">
                            <span class="border-bottom">{{ __("Product List") }}</span>
                            <button type="button" class="add-product fs-6 btn btn-sm btn-outline-danger float-start" data-bs-toggle="modal" data-bs-target="#addProductModal">{{ __("Add Product") }}</button>
                        </h3>

                        <hr>

                        <table id="dataTable" class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>{{ __("SKU") }}</td>
                                    <td>{{ __("Product Name") }}</td>
                                    <td>{{ __("Stock") }}</td>
                                    <td>{{ __("Last Buy Price") }}</td>
                                    <td>{{ __("Corporation price") }}</td>
                                    <td>{{ __("Customer Price") }}</td>
                                    <td style="width:180px"></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Models\Product::all() as $product)
                                    <tr id="product-{{$product->id}}" class="text-center">
                                        <td>{{ $loop->iteration }}</td>
                                        <td>
                                            <button class="text-black btn btn-light bg-transparent border-0" data-bs-toggle="modal" data-bs-target="#editModal"
                                               data-product-id="{{$product->id}}"
                                               data-product-name="{{$product->name}}"
                                               data-product-sku="{{$product->Code}}"
                                               data-product-stock="{{$product->stock}}"
                                               data-product-price="{{$product->price}}"
                                               data-product-co-price="{{$product->co_price}}"
                                               data-product-cu-price="{{$product->customer_price}}"
                                            >
                                                {{ $product->Code }}
                                            </button>
                                        </td>
                                        <td>{{ $product->name }}</td>
                                        <td><span class="stock">{{ $product->stock }}</span></td>
                                        <td><span class="price">{{ fa_price($product->price) }}</span></td>
                                        <td><span class="co_price">{{ fa_price($product->co_price) }}</span></td>
                                        <td><span class="customer_price">{{ fa_price($product->customer_price) }}</span></td>
                                        <td class="text-center">
                                            <button type="button" class="ms-2 btn btn-sm btn-outline-success" data-bs-toggle="modal" data-bs-target="#sellModal"
                                                    data-product-id="{{$product->id}}" data-product-name="{{$product->name}}">{{ __("Sell") }}</button>
                                            <button type="button" class="btn btn-sm btn-outline-warning" data-bs-toggle="modal" data-bs-target="#addStockModal"
                                                    data-product-id="{{$product->id}}" data-product-name="{{$product->name}}">{{ __("Add Stock") }}</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="addProductModal" tabindex="-1" aria-labelledby="addProductModal" data-bs-backdrop="static" data-bs-keyboard="false"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addProductModalLabel">{{ __("Add Product") }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route("product.store") }}" method="post" style="direction: rtl; text-align: right">
                            @csrf
                            <div class="form-group mb-3">
                                <label for="code">{{ __("SKU") }}</label>
                                <input name="code" style="font-family: Arial, serif; text-align: left" id="code" type="text" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="name">{{ __("Product Name") }}</label>
                                <input name="name" required id="name" autocomplete="off" type="text" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="stock">{{ __("Stock") }}</label>
                                <input name="stock" required id="stock" type="number" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="price">{{ __("Buy Price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="price" required  id="price" type="number" class="form-control" >
                                </div>

                            </div>
                            <div class="form-group mb-3">
                                <label for="co_price">{{ __("Corporation price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="co_price" id="co_price" type="number" class="form-control" >
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="customer_price">{{ __("Customer Price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="customer_price" required id="customer_price" type="number" class="form-control" >
                                </div>
                            </div>

                            <div class="border-top pt-3 text-center">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __("Close") }}</button>
                                <button type="submit" id="#submitAddProduct" class="btn btn-danger">{{ __("Add Product") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="sellModal" tabindex="-1" aria-labelledby="sellModal" data-bs-backdrop="static" data-bs-keyboard="false"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="sellModalLabel">{{ __("Sell Product") }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="sellProduct" action="{{ route("product.store") }}" method="post" style="direction: rtl; text-align: right">
                            @csrf
                            <input type="hidden" name="product_id" value="0">
                            <div class="form-group mb-3">
                                <label for="name">{{ __("Product Name") }}</label>
                                <input name="name" disabled id="name" autocomplete="off" type="text" class="form-control" >
                            </div>

                            <div class="form-group mb-3">
                                <label for="sellType">{{ __("Customer Type") }}</label>
                                <select name="customer_type" id="sellType" class="form-control">
                                    <option selected value="normal">{{ __("Person") }}</option>
                                    <option value="corporation">{{ __("Corporation") }}</option>
                                </select>
                            </div>

                            <div class="form-group mb-3">
                                <label for="count">{{ __("Count") }}</label>
                                <input name="count" required id="count" type="number" class="form-control" >
                            </div>

                            <div class="form-group mb-3">
                                <label for="customer_price">{{ __("Sell Price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="customer_price" required id="customer_price" type="number" class="form-control" >
                                </div>
                            </div>

                            <div class="border-top pt-3 text-center">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __("Close") }}</button>
                                <button type="submit" id="#submitSellProduct" class="btn btn-danger">{{ __("Register Order") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="addStockModal" tabindex="-1" aria-labelledby="addStockModal" data-bs-backdrop="static" data-bs-keyboard="false"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="addStockModalLabel">{{ __("Add Product Stock") }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="addProductStock" method="post" style="direction: rtl; text-align: right">
                            @csrf
                            <input type="hidden" name="product_id" value="0">
                            <div class="form-group mb-3">
                                <label for="nameAddStockProduct">{{ __("Product Name") }}</label>
                                <input name="name" disabled id="nameAddStockProduct" autocomplete="off" type="text" class="form-control" >
                            </div>

                            <div class="form-group mb-3">
                                <label for="stockAddStockProduct">{{ __("Count") }}</label>
                                <input name="count" required id="stockAddStockProduct" type="number" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="priceAddStockProduct">{{ __("Buy Price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="price" required  id="priceAddStockProduct" type="number" class="form-control" >
                                </div>

                            </div>
                            <div class="form-group mb-3">
                                <label for="co_priceAddProductStock">{{ __("Corporation price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="co_price" id="co_priceAddProductStock" type="number" class="form-control" value="0" >
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="customer_priceAddProductStock">{{ __("Customer Price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="customer_price" required id="customer_priceAddProductStock" type="number" class="form-control" value="0" >
                                </div>
                            </div>

                            <div class="border-top pt-3 text-center">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __("Close") }}</button>
                                <button type="submit" id="#submitAddProductStock" class="btn btn-danger">{{ __("Register Order") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="editModal" data-bs-backdrop="static" data-bs-keyboard="false"  aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="editModalLabel">{{ __("Edit Product") }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="editProductForm" method="post" style="direction: rtl; text-align: right">
                            @csrf

                            <div class="form-group mb-3">
                                <label for="skuEditProduct">{{ __("SKU") }}</label>
                                <input name="code" id="skuEditProduct" autocomplete="off" type="text" class="form-control" >
                            </div>

                            <input type="hidden" name="product_id" value="0">
                            <div class="form-group mb-3">
                                <label for="nameEditProduct">{{ __("Product Name") }}</label>
                                <input name="name" id="nameEditProduct" autocomplete="off" type="text" class="form-control" >
                            </div>

                            <div class="form-group mb-3">
                                <label for="stockEditProduct">{{ __("Count") }}</label>
                                <input name="stock" required id="stockEditProduct" type="number" class="form-control" >
                            </div>
                            <div class="form-group mb-3">
                                <label for="priceEditProduct">{{ __("Buy Price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="price" required  id="priceEditProduct" type="number" class="form-control" >
                                </div>

                            </div>
                            <div class="form-group mb-3">
                                <label for="co_priceEditProduct">{{ __("Corporation price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="co_price" id="co_priceEditProduct" type="number" class="form-control" value="0" >
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <label for="customer_priceEditProduct">{{ __("Customer Price") }}</label>
                                <div class="input-group" style="direction: ltr">
                                    <span class="input-group-text">{{ __("$") }}</span>
                                    <input name="customer_price" required id="customer_priceEditProduct" type="number" class="form-control" value="0" >
                                </div>
                            </div>

                            <div class="border-top pt-3 text-center">
                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">{{ __("Close") }}</button>
                                <button type="submit" id="#submitAddProductStock" class="btn btn-danger">{{ __("Register Order") }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- Footer !-->
        <script>
            $("#dataTable").dataTable({
                
               language: {
                    "sEmptyTable":     "{{ __("No data available in table") }}",
                    "sInfo":           "{{ __("Showing [ _START_ to _END_ ] From _TOTAL_ Row") }}",
                    "sInfoEmpty":      "{{ __("Search Empty!") }}",
                    "sInfoFiltered":   "{{ __("(filtered from _MAX_ total entries)") }}",
                    "sInfoPostFix":    "",
                    "sInfoThousands":  ",",
                    "sLengthMenu":     "{{ __("Show _MENU_ Row") }}",
                    "sLoadingRecords": "{{ __("Loading...") }}",
                    "sProcessing":     "{{ __("Processing...") }}",
                    "sSearch":         "{{ __("Search") }} : ",
                    "sZeroRecords":    "{{ __("Nothing found with search criteria.") }}",
                    "oPaginate": {
                        "sFirst":    "{{ __("First Page") }}",
                        "sLast":     "{{ __("Last Page") }}",
                        "sNext":     "{{ __("Next") }}",
                        "sPrevious": "{{ __("Previous") }}"
                    },
                    "oAria": {
                        "sSortAscending":  ": {{ __("activate to sort column ascending") }}",
                        "sSortDescending": ": {{ __("activate to sort column descending") }}"
                    }
                }
            });


            @if(Session::has('message'))
                Swal.fire({
                    icon: '{{ Session::get("icon") }}',
                    html: '<p style="direction: rtl; text-align: center">{{ Session::get("message") }}</p>'
                })
            @endif

            var sellModel = document.getElementById('sellModal')
            sellModel.addEventListener('show.bs.modal', function (event) {
                // Button that triggered the modal
                var button = event.relatedTarget
                // Extract info from data-* attributes
                var productName = button.getAttribute('data-product-name')
                var productId = button.getAttribute('data-product-id')
                // If necessary, you could initiate an AJAX request here
                // and then do the updating in a callback.
                //
                // Update the modal's content.
                var modalProductId = sellModel.querySelector('.modal-body input[name=product_id]')
                modalProductId.value = productId

                var modalProductName = sellModel.querySelector('.modal-body input[name=name]')
                modalProductName.value = productName
            })

            $("#sellProduct").on("submit", function (e) {
                e.preventDefault();
                var form = $(e.target);
                $.post("{{ route('stock.store') }}", form.serializeArray())
                    .then(function (response) {
                        if (response.success) {
                            $(sellModel).modal('hide')
                            $(`#product-${response.product}`).find("span.stock").text(response.stock)
                            return Swal.fire({
                                icon: "success",
                                text: "{{ __("Sell Registered.") }}"
                            })
                        } else {
                            return Swal.fire({
                                icon: "error",
                                text: "{{ __("An error occurred.") }}"
                            })
                        }
                    })
                    .catch(function (failure) {
                        return Swal.fire({
                            icon: "error",
                            text: "{{ __("System error occurred.") }}"
                        })
                    })
            })


            var addStockModel = document.getElementById('addStockModal')
            addStockModel.addEventListener('show.bs.modal', function (event) {
                // Button that triggered the modal
                var button = event.relatedTarget
                // Extract info from data-* attributes
                var productId = button.getAttribute('data-product-id')
                var productName = button.getAttribute('data-product-name')
                console.log(productName);
                // If necessary, you could initiate an AJAX request here
                // and then do the updating in a callback.
                //
                // Update the modal's content.
                var modalProductId = addStockModel.querySelector('.modal-body input[name=product_id]')
                modalProductId.value = productId

                var modalProductName = addStockModel.querySelector('.modal-body input[name=name]')
                modalProductName.value = productName
            })

            $("#addProductStock").on("submit", function (e) {
                e.preventDefault();
                var form = $(e.target);
                $.post("{{ route('stock.addMoreStock') }}", form.serializeArray())
                    .then(function (response) {
                        if (response.success) {
                            $(addStockModel).modal('hide')
                            var $productRow = $(`#product-${response.product}`)
                            $productRow.find("span.stock").text(response.stock)
                            $productRow.find("span.price").text(response.price)
                            $productRow.find("span.co_price").text(response.co_price)
                            $productRow.find("span.customer_price").text(response.customer_price)
                            return Swal.fire({
                                icon: "success",
                                text: "{{ __("Stock Updated.") }}"
                            })
                        } else {
                            return Swal.fire({
                                icon: "error",
                                text: "{{ __("An error occurred.") }}"
                            })
                        }
                    })
                    .catch(function (failure) {
                        return Swal.fire({
                            icon: "error",
                            text: "{{ __("System error occurred.") }}"
                        })
                    })
            })


            var editModel = document.getElementById('editModal')
            editModel.addEventListener('show.bs.modal', function (event) {
                // Button that triggered the modal
                var button = event.relatedTarget
                // Extract info from data-* attributes
                var productId = button.getAttribute('data-product-id'),
                    productName = button.getAttribute('data-product-name'),
                    productSku = button.getAttribute('data-product-sku'),
                    productStock = button.getAttribute('data-product-stock'),
                    productPrice = button.getAttribute('data-product-price'),
                    productCoPrice = button.getAttribute('data-product-co-price'),
                    productCuPrice = button.getAttribute('data-product-cu-price')

                // Update the modal's content.
                var modalProductId = editModel.querySelector('.modal-body input[name=product_id]'),
                    modalProductName = editModel.querySelector('.modal-body input[name=name]'),
                    modalProductSku = editModel.querySelector('.modal-body input[name=code]'),
                    modalProductStock = editModel.querySelector('.modal-body input[name=stock]'),
                    modalProductPrice = editModel.querySelector('.modal-body input[name=price]'),
                    modalProductCoPrice = editModel.querySelector('.modal-body input[name=co_price]'),
                    modalProductCuPrice = editModel.querySelector('.modal-body input[name=customer_price]')

                console.log(productStock);

                modalProductId.value = productId
                modalProductName.value = productName
                modalProductSku.value = productSku
                modalProductStock.value = productStock
                modalProductPrice.value = productPrice
                modalProductCoPrice.value = productCoPrice
                modalProductCuPrice.value = productCuPrice

            })

            $("#editProductForm").on("submit", function (e) {
                e.preventDefault();
                var form = $(e.target);
                $.post("{{ route('stock.editWithPostMethod') }}", form.serializeArray())
                    .then(function (response) {
                        if (response.success) {
                            $(editModel).modal('hide')
                            var $productRow = $(`#product-${response.product}`)
                            $productRow.find("span.stock").text(response.stock)
                            $productRow.find("span.price").text(response.price)
                            $productRow.find("span.co_price").text(response.co_price)
                            $productRow.find("span.customer_price").text(response.customer_price)
                            return Swal.fire({
                                icon: "success",
                                text: "{{ __("Product Updated.") }}"
                            })
                        } else {
                            return Swal.fire({
                                icon: "error",
                                text: "{{ __("An error occurred.") }}"
                            })
                        }
                    })
                    .catch(function (failure) {
                        return Swal.fire({
                            icon: "error",
                            text: "{{ __("System error occurred.") }}"
                        })
                    })
            })

        </script>
    </body>
</html>
