<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::find($request->product_id);
        $product->update([
            'sold' => $product->sold + $request->count,
            'stock' => $product->stock - $request->count
        ]);

        Stock::create([
            'product_id' => $product->id,
            'type' => 'out',
            'stock' => $request->count,
            'price' => null,
            'co_price' => $request->customer_type === "normal" ? $request->customer_price : null,
            'customer_price' => $request->customer_type !== "normal" ? $request->customer_price : null
        ]);
        return response([
            'success' => true,
            'product' => $product->id,
            'stock' => $product->stock
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addProductsStock(Request $request)
    {
        $product = Product::find($request->product_id);
        $product->stock = $product->stock + $request->count ;
        $product->price = $product->price < $request->price ? $request->price : $product->price ;
        $product->co_price = $product->co_price < $request->co_price ? $request->co_price : $product->co_price ;
        $product->customer_price = $product->customer_price < $request->customer_price ? $request->customer_price : $product->customer_price ;
        $product->save();

        Stock::create([
            'product_id' => $product->id,
            'type' => 'in',
            'stock' => $request->count,
            'price' => $request->price,
            'co_price' => $request->co_price,
            'customer_price' => $request->customer_price
        ]);
        return response([
            'success' => true,
            'product' => $product->id,
            'stock' => $product->stock,
            'price' => fa_price($product->price),
            'co_price' => fa_price($product->co_price),
            'customer_price' => fa_price($product->customer_price),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function show(Stock $stock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function edit(Stock $stock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Stock $stock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Stock  $stock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Stock $stock)
    {
        //
    }
}
