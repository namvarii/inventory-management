<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Stock;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        if (!$request->code) {
            generateAgain:
            $data['code'] = \Str::slug($request->title, '-') . rand(999,99999) ;
            if (Product::whereCode($data['code'])->count() > 0) { goto generateAgain; }
        } else {
            if (Product::whereCode($data['code'])->count() > 0) { goto generateAgain; }
        }
        $data['description'] = "";
        $produt = Product::create($data);
        Stock::create([
            "type" => "in",
            "product_id" => $produt->id,
            "stock" => $request->stock,
            "price" => $request->price,
            "co_price" => $request->co_price,
            "customer_price" => $request->customer_price,
        ]);
        
        return redirect("/")
                ->with([
                        "message" => __("Product successfully crated."),
                        "icon" => "success"
                ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    public function editWithPostMethod(Request $request)
    {
        $data = $request->except('_token','product_id');
        if (!$request->code) {
            generateAgain:
            $data['code'] = \Str::slug($request->title, '-') . rand(999,99999) ;
            if (Product::whereCode($data['code'])->count() > 0) { goto generateAgain; }
        } else {
            if (Product::whereCode($data['code'])->count() > 0) { goto generateAgain; }
        }
        $data['description'] = "";
        $product = Product::find($request->product_id);
        $oldProductStock = $product->stock;
        $product->update($data);

        if ($oldProductStock != $request->stock) {
            Stock::create([
                "type" => $oldProductStock < $request->stock ? "in" : "out",
                "product_id" => $product->id,
                "stock" => $oldProductStock > $request->stock ? $oldProductStock - $request->stock : $request->stock - $oldProductStock,
                "price" => $request->price,
                "co_price" => $request->co_price,
                "customer_price" => $request->customer_price,
        ]);
        }


        return response([
            'success' => true,
            'product' => $product->id,
            'stock' => $product->stock,
            'price' => fa_price($product->price),
            'co_price' => fa_price($product->co_price),
            'customer_price' => fa_price($product->customer_price),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
