<?php

    if (! function_exists('fa_numbers')) {
        function fa_numbers($string)
        {
            $en = array("0","1","2","3","4","5","6","7","8","9");
            $fa = array("۰","۱","۲","۳","۴","۵","۶","۷","۸","۹");
            return str_replace($en, $fa, $string);
        }
    }
    if (! function_exists('fa_price')) {
        function fa_price($string,$unit = 'تومان')
        {
            return fa_numbers(number_format((int) $string,0,'','،')).' '.$unit;
        }
    }

