<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('Code')->unique();
            $table->string('name');
            $table->text('description')->default(null);

            $table->unsignedBigInteger('stock')->default(0);
            $table->unsignedBigInteger('sold')->default(0);
            $table->unsignedBigInteger('price')->default(0);
            $table->unsignedBigInteger('co_price')->default(0);
            $table->unsignedBigInteger('customer_price')->default(0);

            $table->unsignedTinyInteger('status')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
