<?php

    use App\Http\Controllers\ProductController;
    use App\Http\Controllers\StockController;
    use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource("product", ProductController::class);
    Route::post("product/editWithPostMethod", [ProductController::class, "editWithPostMethod"])->name("stock.editWithPostMethod");
Route::resource("stock", StockController::class);
Route::post("stock/addStock", [StockController::class, "addProductsStock"])->name("stock.addMoreStock");
